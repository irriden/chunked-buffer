# Chunked Buffer

A deque style buffer backed by non-contiguous chunks of memory.

The buffer grows incrementally without re-allocations and can also be consumed incrementally, releasing memory
as it is consumed.

This structure is useful for memory constrained environments.  It limits the size of contiguous
allocations and incrementally releases memory as the buffer is consumed.

Supports `no_std` environments with `alloc`.

## Usage

```rust
use chunked_buffer::ChunkedBuffer;

fn doit() { 
    let mut buf = ChunkedBuffer::new();
    buf.write(&[1, 2, 3]);
    let mut dest = [0; 10];
    let n = buf.read(&mut dest);
    
    assert_eq!(n, 3);
    assert_eq!(dest, [1, 2, 3, 0, 0, 0, 0, 0, 0, 0]);
}
```
