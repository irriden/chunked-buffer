#![cfg_attr(not(feature = "repro"), no_main)]
#![allow(dead_code)]

use std::collections::VecDeque;
use libfuzzer_sys::{Corpus, fuzz_target};
use chunked_buffer::GenericChunkedBuffer;

type Buf = GenericChunkedBuffer<4>;

fn run(mut data: &[u8]) {
	let mut buf = Buf::new();
	let mut deq = VecDeque::<u8>::new();
	let mut counter: usize = 0;
	while data.len() >= 2 || !deq.is_empty() {
		let read_len = if !data.is_empty() {
			let write_len = data[0] as usize;
			data = &data[1..];
			let inp: Vec<u8> = (counter..counter + write_len).map(|i| (i & 0xff) as u8).collect();
			buf.write(&inp);
			deq.extend(inp);
			counter += write_len;
			let read_len = data[0] as usize;
			data = &data[1..];
			read_len
		} else {
			// drain by reading 3 bytes at a time while there's no fuzzing data
			3
		};

		let mut dest_deq = vec![0u8; read_len as usize];
		let mut actual_len = 0;
		for i in 0..read_len {
			if let Some(b) = deq.pop_front() {
				dest_deq[i as usize] = b;
				actual_len += 1;
			} else {
				break;
			}
		}
		let mut dest = vec![0u8; read_len as usize];
		let n = buf.read(&mut dest);
		assert_eq!(n, actual_len);
		assert_eq!(dest, dest_deq);
	}
}

fuzz_target!(|data: &[u8]| -> Corpus {
	if data.len() < 2 || data.len() % 2 != 0 {
		return Corpus::Reject;
	}
    run(data);
	Corpus::Keep
});

// look for Base64: in the crash output from cargo-fuzz
const INPUT: &str = "//8AAAAu";

// Reproduce a crash from cargo-fuzz
#[cfg(feature = "repro")]
fn main() {
	use base64::Engine;

	let data = base64::engine::general_purpose::STANDARD.decode(INPUT).unwrap();
	run(&data);
}
